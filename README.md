Radikal Redirector
==================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting an image hosting page at [Radikal.RU](https://radikal.ru/) -- will instantly redirect to the image itself.
