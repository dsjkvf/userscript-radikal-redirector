// ==UserScript==
// @name        radikal.ru: redirect this bitch
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-radikal-redirector/
// @downloadURL https://bitbucket.org/dsjkvf/userscript-radikal-redirector/raw/master/rrtb.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-radikal-redirector/raw/master/rrtb.user.js
// @match       *://*.radikal.ru/*
// @run-at      document-start
// @version     1.0.2
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;


// HELPERS

function redirectToPrediction(url) {
    return url.replace(/radikal\.ru\/lfp\/(.*)\/htm/, "$1")
}

// MAIN

// Immediately redirect to today if an url doesn't contain a link to a date
if ( /png\/htm$/.test(url)) {
    window.location.replace(redirectToPrediction(url));
}
